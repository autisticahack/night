package com.example.kevin.autismapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageButton;

public class Reason extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        //FULL SCREEN
        requestWindowFeature(Window.FEATURE_NO_TITLE);// hide statusbar of Android
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        //SET VIEW
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reason);

        //INITIALISE VARIABLES
        ImageButton changeButton = (ImageButton) findViewById(R.id.changeButton);
        ImageButton lightButton = (ImageButton) findViewById(R.id.lightButton);
        ImageButton noiseButton = (ImageButton) findViewById(R.id.noiseButton);
        ImageButton thirstyButton = (ImageButton) findViewById(R.id.thirstyButton);
        ImageButton worriesButton = (ImageButton) findViewById(R.id.worriesButton);
        ImageButton unknownButton = (ImageButton) findViewById(R.id.unknownButton);

        changeButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                finish();
                startActivity(new Intent(Reason.this, Method.class));
            }
        });

        lightButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                finish();
                startActivity(new Intent(Reason.this, Method.class));
            }
        });

        noiseButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                finish();
                startActivity(new Intent(Reason.this, Method.class));
            }
        });

        thirstyButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                finish();
                startActivity(new Intent(Reason.this, Method.class));
            }
        });

        worriesButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                finish();
                startActivity(new Intent(Reason.this, Method.class));
            }
        });

        unknownButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                finish();
                startActivity(new Intent(Reason.this, Method.class));
            }
        });

    }
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event){
        if (keyCode == KeyEvent.KEYCODE_BACK)
        {
            Intent toBack = new Intent(Reason.this, MainActivity.class);
            finish();
            startActivity(toBack);
        }
        return false;
    }
}

package com.example.kevin.autismapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import java.util.Timer;
import java.util.TimerTask;

public class CountUpToTen extends AppCompatActivity
{
    Timer timer;
    int countdown;
    TextView timerTextView;
    Button startButton;
    Boolean start;



    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        //FULL SCREEN
        requestWindowFeature(Window.FEATURE_NO_TITLE);// hide statusbar of Android
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        //CONTENT
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_count_up_to_ten);
        timerTextView = (TextView) findViewById(R.id.timerTextView);
        startButton = (Button) findViewById(R.id.startButton);

        timer = new Timer();
        countdown = 0;
        start = true;

        startButton.setText("RESET");


        startButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                if(start)
                {
                    start = false;
                    countdown = 0;
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    start = true;
                }
            }
        });

        //TIMER FOR TIME LEFT

        timer.scheduleAtFixedRate(new TimerTask()
        {
            @Override
            public void run()
            {
                if(countdown < 10 && start)
                {
                    countdown++;
                }
                CountUpToTen.this.runOnUiThread(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        timerTextView.setText(countdown+"");
                    }
                });
            }
        }, 1000, 1000);

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event){
        if (keyCode == KeyEvent.KEYCODE_BACK)
        {
            Intent toBack = new Intent(CountUpToTen.this, Method.class);
            finish();
            startActivity(toBack);
        }
        return false;
    }
}

package com.example.kevin.autismapp;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageButton;

public class Method extends AppCompatActivity {

    private static final String CHOSEN_METHOD = "method";

    SharedPreferences prefs;
    String option;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        //FULL SCREEN
        requestWindowFeature(Window.FEATURE_NO_TITLE);// hide statusbar of Android
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        //SET VIEW
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_method);

        //INITIALISE VARIABLES
        ImageButton breatheButton = (ImageButton) findViewById(R.id.BreatheButton);
        ImageButton breakButton = (ImageButton) findViewById(R.id.BreakButton);
        ImageButton happyPlaceButton = (ImageButton) findViewById(R.id.HappyPlaceButton);
        ImageButton countButton = (ImageButton) findViewById(R.id.CountTo10Button);
        ImageButton playButton = (ImageButton) findViewById(R.id.PlayMusicButton);
        ImageButton helpMeButton = (ImageButton) findViewById(R.id.HelpMe);

        prefs = this.getSharedPreferences("myPrefsKey", Context.MODE_PRIVATE);
        final SharedPreferences.Editor editor = prefs.edit();

        breatheButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                editor.putString(CHOSEN_METHOD, "breathe").apply();
                finish();
                startActivity(new Intent(Method.this, Breathe.class));
            }
        });

        breakButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                option="break";
                editor.putString(CHOSEN_METHOD, "break").apply();
                finish();
                startActivity(new Intent(Method.this, TakeABreak.class));
            }
        });

        happyPlaceButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                editor.putString(CHOSEN_METHOD, "happyplace");
                finish();
                startActivity(new Intent(Method.this, HappyPlace.class));
            }
        });

        countButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                editor.putString(CHOSEN_METHOD, "count");
                finish();
                startActivity(new Intent(Method.this, CountUpToTen.class));
            }
        });

        playButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                editor.putString(CHOSEN_METHOD, "song");
                finish();
                startActivity(new Intent(Method.this, Player.class));
            }
        });

        helpMeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                editor.putString(CHOSEN_METHOD, "helpme").apply();
                finish();
                startActivity(new Intent(Method.this, HelpMe.class));
            }
        });

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event){
        if (keyCode == KeyEvent.KEYCODE_BACK)
        {
            Intent toBack = new Intent(Method.this, Reason.class);
            finish();
            startActivity(toBack);
        }
        return false;
    }
}
package com.example.kevin.autismapp;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.orhanobut.simplelistview.SimpleListView;


public class Player extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_player);

        SimpleListView listView = (SimpleListView) findViewById(R.id.list);

        ArrayAdapter<String> adapter = new ArrayAdapter<>(
                this, android.R.layout.simple_list_item_1,
                new String[]{
                        "song1",
                        "song2",
                        "song3",
                        "song4",
                        "song5",
                        "song6",
                        "song7",
                        "song8",
                        "song9",
                        "song10",
                        "song11",
                        "song12",
                        "song13"
                }
        );

        listView.setHeaderView(R.layout.header);
        listView.setFooterView(R.layout.footer);
        listView.setDividerView(R.layout.divider);
        listView.setOnItemClickListener(new SimpleListView.OnItemClickListener() {
            @Override
            public void onItemClick(Object item, View view, int position) {
                Intent toMusic = new Intent(Player.this, Music.class);
                finish();
                startActivity(toMusic);
            }
        });
        listView.setAdapter(adapter);

        listView.setOnTouchListener(new ListView.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int action = event.getAction();
                switch (action) {
                    case MotionEvent.ACTION_DOWN:
                        // Disallow ScrollView to intercept touch events.
                        v.getParent().requestDisallowInterceptTouchEvent(true);
                        break;

                    case MotionEvent.ACTION_UP:
                        // Allow ScrollView to intercept touch events.
                        v.getParent().requestDisallowInterceptTouchEvent(false);
                        break;
                }

                // Handle ListView touch events.
                v.onTouchEvent(event);
                return true;
            }
        });

        listView.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                Intent toMusic = new Intent(Player.this, Music.class);
                finish();
                startActivity(toMusic);
            }
        });

        //It will refresh the listview
        adapter.notifyDataSetChanged();
    }




    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.play_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event){
        if (keyCode == KeyEvent.KEYCODE_BACK)
        {
            Intent toBack = new Intent(Player.this, Method.class);
            finish();
            startActivity(toBack);
        }
        return false;
    }
}

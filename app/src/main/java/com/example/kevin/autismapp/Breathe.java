package com.example.kevin.autismapp;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.widget.VideoView;

import java.util.Timer;
import java.util.TimerTask;

public class Breathe extends AppCompatActivity {
    Timer timer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_breathe);

        final VideoView videoView =
                (VideoView) findViewById(R.id.videoView);
        Uri video = Uri.parse("android.resource://" + getPackageName() + "/"
                + R.raw.breather);
        videoView.setVideoURI(video);

        videoView.start();


        timer = new Timer();

        timer.scheduleAtFixedRate(new TimerTask()
        {
            @Override
            public void run()
            {
                Breathe.this.runOnUiThread(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        if(!videoView.isPlaying())
                        {
                            videoView.start();
                        }
                    }
                });
            }
        }, 10, 10);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event){
        if (keyCode == KeyEvent.KEYCODE_BACK)
        {
            Intent toBack = new Intent(Breathe.this, Method.class);
            finish();
            startActivity(toBack);
        }
        return false;
    }
}

package com.example.kevin.autismapp;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageButton;

public class MainActivity extends AppCompatActivity  {

    private static final String OPTION = "option";

    private SharedPreferences prefs;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        //FULL SCREEN
        requestWindowFeature(Window.FEATURE_NO_TITLE);// hide statusbar of Android
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        //SET VIEW
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        /*Intent firstintent = new Intent(MainActivity.this, LoginActivity.class);
        startActivity(firstintent);*/

        //INITIALISE VARIABLES
        ImageButton sadButton = (ImageButton) findViewById(R.id.sadButton);
        ImageButton angryButton = (ImageButton) findViewById(R.id.angryButton);
        ImageButton scaredButton = (ImageButton) findViewById(R.id.scaredButton);
        ImageButton nervousButton = (ImageButton) findViewById(R.id.nervousButton);
        ImageButton tiredButton = (ImageButton) findViewById(R.id.tiredButton);
        ImageButton surprisedButton = (ImageButton) findViewById(R.id.surprisedButton);

        prefs = this.getSharedPreferences("myPrefsKey", Context.MODE_PRIVATE);
        final SharedPreferences.Editor editor = prefs.edit();


        //BUTTONS
        sadButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                editor.putString(OPTION, "sad").apply();
                finish();
                startActivity(new Intent(MainActivity.this, Reason.class));
            }
        });


        angryButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                editor.putString(OPTION, "angry").apply();
                finish();
                startActivity(new Intent(MainActivity.this, Reason.class));
            }
        });


        scaredButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                editor.putString(OPTION, "scared").apply();
                finish();
                startActivity(new Intent(MainActivity.this, Reason.class));
            }
        });

        nervousButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                editor.putString(OPTION, "nervous").apply();
                finish();
                startActivity(new Intent(MainActivity.this, Reason.class));
            }
        });

        tiredButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                editor.putString(OPTION, "tired").apply();
                finish();
                startActivity(new Intent(MainActivity.this, Reason.class));
            }
        });

        surprisedButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                editor.putString(OPTION, "surprised").apply();
                finish();
                startActivity(new Intent(MainActivity.this, Reason.class));
            }
        });

    }
}

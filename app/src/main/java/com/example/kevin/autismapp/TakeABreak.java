package com.example.kevin.autismapp;

import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Vibrator;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import java.util.Timer;
import java.util.TimerTask;

public class TakeABreak extends AppCompatActivity
{
    //VARIABLES
    TextView counterTextView;
    int countdownNum;
    int min;
    int sec;
    Timer timer;
    Boolean soundHappened;
    Vibrator v;

    MediaPlayer mp;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        //FULL SCREEN
        requestWindowFeature(Window.FEATURE_NO_TITLE);// hide statusbar of Android
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        //SET CONTENT
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_take_abreak);

        //INITIALISE VARIABLES
        counterTextView = (TextView) findViewById(R.id.counterTextView);
        countdownNum = 600;
        min = 10;
        sec = 00;
        timer = new Timer();
        mp = MediaPlayer.create(this, R.raw.bell_sound);
        soundHappened=false;
        v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);



        timer.scheduleAtFixedRate(new TimerTask()
        {
            @Override
            public void run()
            {
                sec--;
                if(sec<0)
                {
                    sec=59;
                    min--;
                    if(min<0)
                    {
                        if(!soundHappened)
                        {
                            v.vibrate(1000);
                            mp.start();
                            soundHappened = true;
                        }
                    }
                }
                else
                {

                }
                TakeABreak.this.runOnUiThread(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        if(min>=0)
                        {
                            if (sec >= 10) {
                                counterTextView.setText("0"+min + ":" + sec);
                            } else
                            {
                                counterTextView.setText("0"+min + ":0" + sec);
                            }
                        }
                        else
                        {
                            if(sec%2==0)
                            {
                                counterTextView.setText("00:00");
                            }
                            else
                            {
                                counterTextView.setText("");
                            }
                        }
                    }
                });
            }
        }, 1000, 1000);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event){
        if (keyCode == KeyEvent.KEYCODE_BACK)
        {
            Intent toBack = new Intent(TakeABreak.this, Method.class);
            finish();
            startActivity(toBack);
        }
        return false;
    }
}
